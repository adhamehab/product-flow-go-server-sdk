package productflow

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestNewAccessControllerSuccess(t *testing.T) {

	val, err := NewAccessController(testApiEndpoint, &http.Client{})
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	if val.apiEndpoint != testApiEndpoint {
		t.Fatalf("apiEndpoint was %v, expected %v", val.apiEndpoint, testApiEndpoint)
	}
}

func TestNewAccessControllerNoApiEndpoint(t *testing.T) {

	_, err := NewAccessController("", &http.Client{})
	if fmt.Sprint(err) != "apiEndpoint must be provided" {
		t.Fatalf("unexpected error: %v", err)
	}
}

func TestAccessControllerStartSuccess(t *testing.T) {

	response := []byte(`{
		"authenticated": true,
		"features": {
			"testfeature1": { "type": "STATIC", "enabled": true },
			"testfeature2": { "type": "DYNAMIC" }
		}
	}`)
	var requestURI string
	var token string
	var body map[string]interface{}
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestURI = r.RequestURI
		token = r.Header.Get("Authorization")
		json.NewDecoder(r.Body).Decode(&body)

		w.WriteHeader(http.StatusOK)
		w.Write(response)
	}))
	defer svr.Close()

	ac, _ := NewAccessController(svr.URL, &http.Client{})
	params := AccessParams{
		Jwt:       "123",
		Ip:        "127.0.0.1",
		UserAgent: "Mozilla/123",
		Path:      "/",
	}
	customInputs := map[string]interface{}{
		"test": true,
	}

	err := ac.Start(params, customInputs)
	switch {
	case err != nil:
		t.Fatalf("could not start access controller: %v", err)
	case !strings.HasSuffix(requestURI, "/access/currentState"):
		t.Fatalf("request url wrong: %v", requestURI)
	case token != "Bearer "+params.Jwt:
		t.Fatalf("authentication bearer token wrong: %v", token)
	case body["ip"] != params.Ip:
		t.Fatalf("request body wrong ip: %v", body["ip"])
	case body["userAgent"] != params.UserAgent:
		t.Fatalf("request body wrong user agent: %v", body["userAgent"])
	case body["path"] != params.Path:
		t.Fatalf("request body wrong path: %v", body["path"])
	case body["test"] != customInputs["test"]:
		t.Fatalf("request body wrong custom input test: %v", body["test"])
	case !ac.IsAuthenticated():
		t.Fatalf("user should be authenticated")
	case len(ac.AllFeatures()) != 2:
		t.Fatalf("Should be 2 features: %v", ac.AllFeatures())
	case !ac.Started:
		t.Fatalf("access controller should be started")
	}
}

func TestAccessControllerStartServerError(t *testing.T) {

	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(``))
	}))
	defer svr.Close()

	ac, _ := NewAccessController(svr.URL, &http.Client{})
	params := AccessParams{Jwt: "123"}

	err := ac.Start(params, nil)
	if err != ErrPFBadResponse {
		t.Fatalf("unexpected error: %v", err)
	}
}

func TestAccessControllerStartNoJwt(t *testing.T) {

	ac, _ := NewAccessController("http://localhost/nowhere", &http.Client{})
	params := AccessParams{}

	err := ac.Start(params, nil)
	if fmt.Sprint(err) != "jwt token must be provided" {
		t.Fatalf("unexpected error: %v", err)
	}
}

func TestAllFeaturesUninit(t *testing.T) {

	ac, _ := NewAccessController("http://localhost/nowhere", &http.Client{})

	features := ac.AllFeatures()
	if len(features) != 0 {
		t.Fatalf("features should be empty %v", features)
	}
}

func TestIsAuthenticatedUninit(t *testing.T) {

	ac, _ := NewAccessController("http://localhost/nowhere", &http.Client{})

	authed := ac.IsAuthenticated()
	if authed {
		t.Fatalf("should not be authenticated")
	}
}

func TestAccessControllerUpdateSuccess(t *testing.T) {

	responseStart := []byte(`{
		"authenticated": false,
		"features": {}
	}`)

	responseUpdate := []byte(`{
		"authenticated": true,
		"features": {
			"testfeature1": { "type": "STATIC", "enabled": true },
			"testfeature2": { "type": "DYNAMIC" }
		}
	}`)
	requestCount := 0
	var requestURI string
	var token string
	var body map[string]interface{}
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestCount++
		var response = responseStart

		if requestCount == 2 {
			requestURI = r.RequestURI
			token = r.Header.Get("Authorization")
			json.NewDecoder(r.Body).Decode(&body)
			response = responseUpdate
		}

		w.WriteHeader(http.StatusOK)
		w.Write(response)
	}))
	defer svr.Close()

	ac, _ := NewAccessController(svr.URL, &http.Client{})
	params := AccessParams{
		Jwt:       "123",
		Ip:        "127.0.0.1",
		UserAgent: "Mozilla/123",
		Path:      "/",
	}
	customInputs := map[string]interface{}{
		"test": true,
	}

	err := ac.Start(params, customInputs)
	if err != nil {
		t.Fatalf("could not start access controller: %v", err)
	}

	err = ac.Update(params, customInputs)

	switch {
	case err != nil:
		t.Fatalf("could not update access controller: %v", err)
	case !strings.HasSuffix(requestURI, "/access/currentState"):
		t.Fatalf("request url wrong: %v", requestURI)
	case token != "Bearer "+params.Jwt:
		t.Fatalf("authentication bearer token wrong: %v", token)
	case body["ip"] != params.Ip:
		t.Fatalf("request body wrong ip: %v", body["ip"])
	case body["userAgent"] != params.UserAgent:
		t.Fatalf("request body wrong user agent: %v", body["userAgent"])
	case body["path"] != params.Path:
		t.Fatalf("request body wrong path: %v", body["path"])
	case body["test"] != customInputs["test"]:
		t.Fatalf("request body wrong custom input test: %v", body["test"])
	case !ac.IsAuthenticated():
		t.Fatalf("user should be authenticated")
	case len(ac.AllFeatures()) != 2:
		t.Fatalf("Should be 2 features: %v", ac.AllFeatures())
	case !ac.Started:
		t.Fatalf("access controller should be started")
	}
}

func TestAccessControllerUpdateUninit(t *testing.T) {

	ac, _ := NewAccessController("http://localhost/nowhere", &http.Client{})

	err := ac.Update(AccessParams{}, nil)
	if fmt.Sprint(err) != "access controller not started" {
		t.Fatalf("unexpected error: %v", err)
	}
}

func TestIsFeatureEnabledStatic(t *testing.T) {

	response := []byte(`{
		"authenticated": true,
		"features": {
			"testfeature1": { "type": "STATIC", "enabled": true }
		}
	}`)
	requestCount := 0
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestCount++

		w.WriteHeader(http.StatusOK)
		w.Write(response)
	}))
	defer svr.Close()

	ac, _ := NewAccessController(svr.URL, &http.Client{})
	params := AccessParams{Jwt: "123"}
	customInputs := map[string]interface{}{
		"test": true,
	}

	err := ac.Start(params, customInputs)
	if err != nil {
		t.Fatalf("could not start access controller: %v", err)
	}

	enabled := ac.IsFeatureEnabled("testfeature1", customInputs, false)
	if !enabled {
		t.Fatalf("feature should be enabled: %v", enabled)
	}
	if requestCount != 1 {
		t.Fatalf("static feature should not make additional api requests: %v", requestCount)
	}
}

func TestIsFeatureEnabledDynamic(t *testing.T) {

	responseStart := []byte(`{
		"authenticated": true,
		"features": {
			"testfeature2": { "type": "DYNAMIC" }
		}
	}`)
	responseFeature := []byte(`{
		"featureSlug": "testfeature2",
		"enabled": true
	}`)
	requestCount := 0
	var body map[string]interface{}
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestCount++
		json.NewDecoder(r.Body).Decode(&body)

		w.WriteHeader(http.StatusOK)
		if strings.HasSuffix(r.RequestURI, "/access/currentState") {
			w.Write(responseStart)
		} else if strings.HasSuffix(r.RequestURI, "/access/feature/testfeature2") {
			w.Write(responseFeature)
		}
	}))
	defer svr.Close()

	ac, _ := NewAccessController(svr.URL, &http.Client{})
	params := AccessParams{Jwt: "123"}
	customInputs := map[string]interface{}{
		"test": true,
	}

	err := ac.Start(params, customInputs)
	if err != nil {
		t.Fatalf("could not start access controller: %v", err)
	}

	enabled := ac.IsFeatureEnabled("testfeature2", customInputs, false)
	switch {
	case !enabled:
		t.Fatalf("feature should be enabled: %v", enabled)
	case requestCount != 2:
		t.Fatalf("dynamic feature should make 1 additional api requests: %v", requestCount)
	case body["test"] != customInputs["test"]:
		t.Fatalf("request body wrong custom input test: %v", body["test"])
	}
}

func TestIsFeatureEnabledDefaultFallback(t *testing.T) {

	ac, _ := NewAccessController("http://localhost/nowhere", &http.Client{})

	enabled := ac.IsFeatureEnabled("testfeature3", nil, true)
	if !enabled {
		t.Fatalf("feature should be default enabled: %v", enabled)
	}
}

func TestGetRuleDecisionSuccess(t *testing.T) {

	responseStart := []byte(`{
		"authenticated": true,
		"features": {}
	}`)
	responseDecision := []byte(`{
		"ruleSlug": "testrule",
		"outputType": "STRING",
		"outputValue": "Testing 123"
	}`)
	requestCount := 0
	var body map[string]interface{}
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestCount++

		w.WriteHeader(http.StatusOK)
		if strings.HasSuffix(r.RequestURI, "/access/currentState") {
			w.Write(responseStart)
		} else if strings.HasSuffix(r.RequestURI, "/access/decision/testrule") {
			json.NewDecoder(r.Body).Decode(&body)
			w.Write(responseDecision)
		}
	}))
	defer svr.Close()

	ac, _ := NewAccessController(svr.URL, &http.Client{})
	params := AccessParams{Jwt: "123"}
	customInputs := map[string]interface{}{
		"test": true,
	}

	ac.Start(params, customInputs)

	decision, err := ac.GetRuleDecision("testrule", customInputs)
	switch {
	case err != nil:
		t.Fatalf("could not get rule decision: %v", err)
	case decision.Slug != "testrule":
		t.Fatalf("rule decision slug not expected: %v", decision.Slug)
	case decision.OutputType != "STRING":
		t.Fatalf("rule decision type not expected: %v", decision.OutputType)
	case requestCount != 2:
		t.Fatalf("rule deicion should make 1 additional api requests: %v", requestCount)
	case body["test"] != customInputs["test"]:
		t.Fatalf("request body wrong custom input test: %v", body["test"])
	}

	var outputValue string
	json.Unmarshal(decision.OutputValue, &outputValue)
	if outputValue != "Testing 123" {
		t.Fatalf("rule decision value not expected: %v", outputValue)
	}
}

func TestGetRuleDecisionNotFound(t *testing.T) {

	responseStart := []byte(`{
		"authenticated": true,
		"features": {}
	}`)
	responseDecision := []byte(`{
		"status": 404,
		"type": "NOT_FOUND",
		"message": "rule not found"
	}`)
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if strings.HasSuffix(r.RequestURI, "/access/currentState") {
			w.WriteHeader(http.StatusOK)
			w.Write(responseStart)
		} else if strings.HasSuffix(r.RequestURI, "/access/decision/testrule2") {
			w.WriteHeader(http.StatusNotFound)
			w.Write(responseDecision)
		}
	}))
	defer svr.Close()

	ac, _ := NewAccessController(svr.URL, &http.Client{})
	params := AccessParams{Jwt: "123"}

	ac.Start(params, nil)

	_, err := ac.GetRuleDecision("testrule2", nil)
	if err != ErrPFNotFound {
		t.Fatalf("unexpected error: %v", err)
	}
}

func TestGetRuleDecisionString(t *testing.T) {
	tests := map[string]struct {
		respStatus   int
		respBody     []byte
		expectErr    error
		expectOutput string
	}{
		"success": {
			respStatus: http.StatusOK,
			respBody: []byte(`{
				"ruleSlug": "testrule",
				"outputType": "STRING",
				"outputValue": "Testing 123"
			}`),
			expectOutput: "Testing 123",
		},
		"not found": {
			respStatus: http.StatusNotFound,
			respBody: []byte(`{
				"status": 404,
				"type": "NOT_FOUND",
				"message": "rule not found"
			}`),
			expectErr:    ErrPFNotFound,
			expectOutput: "",
		},
		"enum": {
			respStatus: http.StatusOK,
			respBody: []byte(`{
				"ruleSlug": "testrule",
				"outputType": "ENUM",
				"outputValue": "TEST"
			}`),
			expectOutput: "TEST",
		},
		"number": {
			respStatus: http.StatusOK,
			respBody: []byte(`{
				"ruleSlug": "testrule",
				"outputType": "NUMBER",
				"outputValue": 3
			}`),
			expectErr: ErrPFOutputTypeMismatch,
		},
	}

	responseStart := []byte(`{
		"authenticated": true,
		"features": {}
	}`)

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {

			svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

				if strings.HasSuffix(r.RequestURI, "/access/currentState") {
					w.Write(responseStart)
				} else if strings.HasSuffix(r.RequestURI, "/access/decision/testrule") {
					w.WriteHeader(tc.respStatus)
					w.Write(tc.respBody)
				}
			}))
			defer svr.Close()

			ac, _ := NewAccessController(svr.URL, &http.Client{})
			params := AccessParams{Jwt: "123"}
			customInputs := map[string]interface{}{"test": true}

			ac.Start(params, customInputs)

			outputString, err := ac.GetRuleDecisionString("testrule", customInputs)
			switch {
			case tc.expectErr != err:
				t.Fatalf("expected error: %v, got: %v", tc.expectErr, err)
			case tc.expectOutput != outputString:
				t.Fatalf("expected output: %v, got: %v", tc.expectOutput, outputString)
			}
		})
	}
}

func TestGetRuleDecisionInt(t *testing.T) {
	tests := map[string]struct {
		respStatus   int
		respBody     []byte
		expectErr    error
		expectOutput int
	}{
		"success": {
			respStatus:   http.StatusOK,
			respBody:     []byte(`{"ruleSlug": "testrule", "outputType": "NUMBER", "outputValue": 42}`),
			expectOutput: 42,
		},
		"not found": {
			respStatus: http.StatusNotFound,
			respBody:   []byte(`{"status": 404, "type": "NOT_FOUND", "message": "rule not found"}`),
			expectErr:  ErrPFNotFound,
		},
		"float": {
			respStatus: http.StatusOK,
			respBody:   []byte(`{"ruleSlug": "testrule", "outputType": "NUMBER", "outputValue": 42.7891}`),
			expectErr:  ErrPFOutputTypeMismatch,
		},
		"enum": {
			respStatus: http.StatusOK,
			respBody:   []byte(`{"ruleSlug": "testrule", "outputType": "ENUM", "outputValue": "TEST"}`),
			expectErr:  ErrPFOutputTypeMismatch,
		},
		"string": {
			respStatus: http.StatusOK,
			respBody:   []byte(`{"ruleSlug": "testrule", "outputType": "STRING", "outputValue": "Testing 123"}`),
			expectErr:  ErrPFOutputTypeMismatch,
		},
	}

	responseStart := []byte(`{
		"authenticated": true,
		"features": {}
	}`)

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {

			svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

				if strings.HasSuffix(r.RequestURI, "/access/currentState") {
					w.Write(responseStart)
				} else if strings.HasSuffix(r.RequestURI, "/access/decision/testrule") {
					w.WriteHeader(tc.respStatus)
					w.Write(tc.respBody)
				}
			}))
			defer svr.Close()

			ac, _ := NewAccessController(svr.URL, &http.Client{})
			params := AccessParams{Jwt: "123"}
			customInputs := map[string]interface{}{"test": true}

			ac.Start(params, customInputs)

			outputInt, err := ac.GetRuleDecisionInt("testrule", customInputs)
			switch {
			case tc.expectErr != err:
				t.Fatalf("expected error: %v, got: %v", tc.expectErr, err)
			case tc.expectOutput != outputInt:
				t.Fatalf("expected output: %v, got: %v", tc.expectOutput, outputInt)
			}
		})
	}
}

func TestGetRuleDecisionFloat(t *testing.T) {
	tests := map[string]struct {
		respStatus   int
		respBody     []byte
		expectErr    error
		expectOutput float64
	}{
		"success": {
			respStatus:   http.StatusOK,
			respBody:     []byte(`{"ruleSlug": "testrule", "outputType": "NUMBER", "outputValue": 42.1234}`),
			expectOutput: 42.1234,
		},
		"not found": {
			respStatus: http.StatusNotFound,
			respBody:   []byte(`{"status": 404, "type": "NOT_FOUND", "message": "rule not found"}`),
			expectErr:  ErrPFNotFound,
		},
		"int": {
			respStatus:   http.StatusOK,
			respBody:     []byte(`{"ruleSlug": "testrule", "outputType": "NUMBER", "outputValue": 42}`),
			expectOutput: 42,
		},
		"enum": {
			respStatus: http.StatusOK,
			respBody:   []byte(`{"ruleSlug": "testrule", "outputType": "ENUM", "outputValue": "TEST"}`),
			expectErr:  ErrPFOutputTypeMismatch,
		},
		"string": {
			respStatus: http.StatusOK,
			respBody:   []byte(`{"ruleSlug": "testrule", "outputType": "STRING", "outputValue": "Testing 123"}`),
			expectErr:  ErrPFOutputTypeMismatch,
		},
	}

	responseStart := []byte(`{
		"authenticated": true,
		"features": {}
	}`)

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {

			svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

				if strings.HasSuffix(r.RequestURI, "/access/currentState") {
					w.Write(responseStart)
				} else if strings.HasSuffix(r.RequestURI, "/access/decision/testrule") {
					w.WriteHeader(tc.respStatus)
					w.Write(tc.respBody)
				}
			}))
			defer svr.Close()

			ac, _ := NewAccessController(svr.URL, &http.Client{})
			params := AccessParams{Jwt: "123"}
			customInputs := map[string]interface{}{"test": true}

			ac.Start(params, customInputs)

			outputFloat, err := ac.GetRuleDecisionFloat("testrule", customInputs)
			switch {
			case tc.expectErr != err:
				t.Fatalf("expected error: %v, got: %v", tc.expectErr, err)
			case tc.expectOutput != outputFloat:
				t.Fatalf("expected output: %v, got: %v", tc.expectOutput, outputFloat)
			}
		})
	}
}

func TestReportUserTierChange(t *testing.T) {
	tests := map[string]struct {
		previousTierSlug string
		respStatus       int
		respBody         []byte
		expectErr        error
	}{
		"success": {
			previousTierSlug: "free",
			respStatus:       http.StatusOK,
			respBody:         []byte(`{"ok": true, "message": "Event received successfully"}`),
			expectErr:        nil,
		},
		"no previous tier": {
			respStatus: http.StatusOK,
			respBody:   []byte(`{"ok": true, "message": "Event received successfully"}`),
			expectErr:  nil,
		},
		"bad request": {
			previousTierSlug: "free",
			respStatus:       http.StatusBadRequest,
			respBody:         []byte(`{"status": 400, "type": "BAD_REQUEST", "message": "unexpected character"}`),
			expectErr:        ErrPFBadResponse,
		},
		"not ok": {
			previousTierSlug: "free",
			respStatus:       http.StatusOK,
			respBody:         []byte(`{"ok": false, "message": "could not report user tier change"}`),
			expectErr:        ErrPFBadResponse,
		},
	}

	responseStart := []byte(`{
		"authenticated": true,
		"features": {}
	}`)

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {

			svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

				if strings.HasSuffix(r.RequestURI, "/access/currentState") {
					w.Write(responseStart)
				} else if strings.HasSuffix(r.RequestURI, "/eventReporting/userTierChange") {
					w.WriteHeader(tc.respStatus)
					w.Write(tc.respBody)
				}
			}))
			defer svr.Close()

			ac, _ := NewAccessController(svr.URL, &http.Client{})
			params := AccessParams{Jwt: "123"}
			customInputs := map[string]interface{}{"test": true}

			ac.Start(params, customInputs)

			err := ac.ReportUserTierChange(tc.previousTierSlug)
			if tc.expectErr != err {
				t.Fatalf("expected error: %v, got: %v", tc.expectErr, err)
			}
		})
	}
}
