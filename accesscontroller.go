package productflow

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

type AccessController struct {
	apiEndpoint  string
	accessParams AccessParams
	accessState  AccessState
	httpClient   *http.Client
	Started      bool
}

type AccessParams struct {
	Jwt       string `json:"-"`
	Ip        string `json:"ip"`
	UserAgent string `json:"userAgent"`
	Path      string `json:"path"`
}

type AccessState struct {
	Authenticated bool                          `json:"authenticated"`
	Features      map[string]FeatureAccessState `json:"features"`
}

type FeatureAccessState struct {
	Type    string `json:"type"`
	Enabled bool   `json:"enabled"`
}

type FeatureAccessResponse struct {
	Slug    string `json:"featureSlug"`
	Enabled bool   `json:"enabled"`
}

type RuleDecisionResponse struct {
	Slug        string          `json:"ruleSlug"`
	OutputType  string          `json:"outputType"`
	OutputValue json.RawMessage `json:"outputValue"`
}

type userTierChangeResponse struct {
	Ok      bool   `json:"ok"`
	Message string `json:"message"`
}

var (
	ErrPFConnectionFailed   = errors.New("api connection to product flow failed")
	ErrPFNotFound           = errors.New("product flow api resource not found")
	ErrPFBadResponse        = errors.New("product flow api bad response")
	ErrPFOutputTypeMismatch = errors.New("decision output type mismatch")
)

// NewAccessController creates a new Product Flow access controller instance
func NewAccessController(
	apiEndpoint string,
	httpClient *http.Client,
) (*AccessController, error) {

	if apiEndpoint == "" {
		return nil, errors.New("apiEndpoint must be provided")
	}

	ac := &AccessController{
		apiEndpoint: apiEndpoint,
		httpClient:  httpClient,
		Started:     false,
	}

	return ac, nil
}

// Start the access controller to determine user state
//
// JWT is a required field in params. Passing custom inputs is optional.
func (ac *AccessController) Start(
	params AccessParams,
	customInputs map[string]interface{},
) error {

	if params.Jwt == "" {
		return errors.New("jwt token must be provided")
	}
	if params.Path == "" {
		params.Path = "/"
	}

	ac.accessParams = params

	accessState, err := ac.getCurrentAccessState(customInputs)
	if err != nil {
		return err
	}

	ac.accessState = *accessState
	ac.Started = true

	log.Printf("access controller has started successfully")

	return nil
}

// Update the access controller when user parameters change
//
// Access params passed here overwrite those set previously.
// Passing custom inputs is optional.
func (ac *AccessController) Update(
	params AccessParams,
	customInputs map[string]interface{},
) error {

	if !ac.Started {
		return errors.New("access controller not started")
	}

	if params.Jwt != "" {
		ac.accessParams.Jwt = params.Jwt
	}
	if params.Ip != "" {
		ac.accessParams.Ip = params.Ip
	}
	if params.Path != "" {
		ac.accessParams.Path = params.Path
	}
	if params.UserAgent != "" {
		ac.accessParams.UserAgent = params.UserAgent
	}

	accessState, err := ac.getCurrentAccessState(customInputs)
	if err != nil {
		return err
	}

	ac.accessState = *accessState

	log.Printf("access controller has updated successfully")

	return nil
}

// Check if a feature is enabled for the current user
//
// Uses the feature slug with optional custom inputs to check if enabled.
// If anything goes wrong, will fallback to the defaultEnabled value
func (ac *AccessController) IsFeatureEnabled(
	fslug string,
	customInputs map[string]interface{},
	defaultEnabled bool,
) bool {
	feature, ok := ac.accessState.Features[fslug]
	if !ok {
		log.Printf("feature %v not found, using default: %v\n", fslug, defaultEnabled)
		return defaultEnabled
	}

	switch feature.Type {
	case "STATIC":
		return feature.Enabled
	case "DYNAMIC":
		far, err := ac.runDynamicFeature(fslug, customInputs)
		if err != nil {
			return defaultEnabled
		}
		return far.Enabled
	default:
		log.Printf("feature %v with type %v unrecognised\n", fslug, feature.Type)
		return defaultEnabled
	}
}

// Get a rule decision
//
// Uses the rule slug with optional custom inputs to make a decision.
// Returns a generic RuleDecisionResponse for user to Unmarshal later.
func (ac *AccessController) GetRuleDecision(
	rslug string,
	customInputs map[string]interface{},
) (*RuleDecisionResponse, error) {

	return ac.ruleDecisionRequest(rslug, customInputs)
}

// Get a string rule decision
//
// Uses the rule slug with optional custom inputs to make a decision.
// Returns an error if the decision output value cannot be Unmarshalled.
func (ac *AccessController) GetRuleDecisionString(
	rslug string,
	customInputs map[string]interface{},
) (string, error) {

	resp, err := ac.ruleDecisionRequest(rslug, customInputs)
	if err != nil {
		return "", err
	}

	var output string
	err = json.Unmarshal(resp.OutputValue, &output)
	if err != nil {
		return "", ErrPFOutputTypeMismatch
	}

	return output, nil
}

// Get an int rule decision
//
// Uses the rule slug with optional custom inputs to make a decision.
// Returns an error if the decision output value cannot be Unmarshalled.
func (ac *AccessController) GetRuleDecisionInt(
	rslug string,
	customInputs map[string]interface{},
) (int, error) {

	resp, err := ac.ruleDecisionRequest(rslug, customInputs)
	if err != nil {
		return 0, err
	}

	var output int
	err = json.Unmarshal(resp.OutputValue, &output)
	if err != nil {
		return 0, ErrPFOutputTypeMismatch
	}

	return output, nil
}

// Get a float rule decision
//
// Uses the rule slug with optional custom inputs to make a decision.
// Returns an error if the decision output value cannot be Unmarshalled.
func (ac *AccessController) GetRuleDecisionFloat(
	rslug string,
	customInputs map[string]interface{},
) (float64, error) {

	resp, err := ac.ruleDecisionRequest(rslug, customInputs)
	if err != nil {
		return 0, err
	}

	var output float64
	err = json.Unmarshal(resp.OutputValue, &output)
	if err != nil {
		return 0, ErrPFOutputTypeMismatch
	}

	return output, nil
}

// Report a users tier change
//
// When a users tier has changed, use this function to track in Product Flow.
func (ac *AccessController) ReportUserTierChange(previousTierSlug string) error {

	m := map[string]interface{}{
		"previousTierSlug": previousTierSlug,
	}

	jsonData, err := json.Marshal(m)
	if err != nil {
		return errors.New("failed to marshal payload")
	}

	resp, err := ac.apiRequest("/eventReporting/userTierChange", bytes.NewBuffer(jsonData))
	if err != nil {
		return ErrPFConnectionFailed
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return handleBadResponse(resp)
	}

	var utcr userTierChangeResponse
	err = json.NewDecoder(resp.Body).Decode(&utcr)
	if err != nil {
		log.Printf("failed to decode tier change response, err was: %v", err)
		return ErrPFBadResponse
	}
	if !utcr.Ok {
		log.Printf("user tier change report failed: %v", utcr.Message)
		return ErrPFBadResponse
	}

	return nil
}

// Check if the current user is authenticated
func (ac *AccessController) IsAuthenticated() bool {
	return ac.accessState.Authenticated
}

// Get all of the current users features
func (ac *AccessController) AllFeatures() map[string]FeatureAccessState {
	return ac.accessState.Features
}

func (ac *AccessController) getCurrentAccessState(
	customInputs map[string]interface{},
) (*AccessState, error) {

	jsonData, err := reqJsonData(ac.accessParams, customInputs)
	if err != nil {
		return nil, errors.New("failed to parse access state input data")
	}

	resp, err := ac.apiRequest("/access/currentState", bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, ErrPFConnectionFailed
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, handleBadResponse(resp)
	}

	var as AccessState
	err = json.NewDecoder(resp.Body).Decode(&as)
	if err != nil {
		log.Printf("failed to decode, err was: %v", err)
		return nil, ErrPFBadResponse
	}

	return &as, nil
}

func (ac *AccessController) runDynamicFeature(
	fslug string,
	customInputs map[string]interface{},
) (*FeatureAccessResponse, error) {

	jsonData, err := reqJsonData(ac.accessParams, customInputs)
	if err != nil {
		return nil, errors.New("failed to parse access state input data")
	}

	resp, err := ac.apiRequest("/access/feature/"+fslug, bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, ErrPFConnectionFailed
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, handleBadResponse(resp)
	}

	var far FeatureAccessResponse
	err = json.NewDecoder(resp.Body).Decode(&far)
	if err != nil {
		log.Printf("failed to decode, err was: %v", err)
		return nil, ErrPFBadResponse
	}

	return &far, nil
}

func (ac *AccessController) ruleDecisionRequest(
	rslug string,
	customInputs map[string]interface{},
) (*RuleDecisionResponse, error) {

	jsonData, err := reqJsonData(ac.accessParams, customInputs)
	if err != nil {
		return nil, errors.New("failed to parse access state input data")
	}

	resp, err := ac.apiRequest("/access/decision/"+rslug, bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, ErrPFConnectionFailed
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, handleBadResponse(resp)
	}

	var rdr RuleDecisionResponse
	err = json.NewDecoder(resp.Body).Decode(&rdr)
	if err != nil {
		log.Printf("failed to decode rule response, err was: %v", err)
		return nil, ErrPFBadResponse
	}

	return &rdr, nil
}

func (ac *AccessController) apiRequest(
	path string,
	body io.Reader,
) (*http.Response, error) {

	req, err := http.NewRequest("POST", ac.apiEndpoint+path, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %v", ac.accessParams.Jwt))

	return ac.httpClient.Do(req)
}

func reqJsonData(
	ap AccessParams,
	m map[string]interface{},
) ([]byte, error) {

	if m == nil {
		m = make(map[string]interface{})
	}

	var apMap map[string]interface{}
	apRaw, err := json.Marshal(ap)
	if err != nil {
		return nil, err
	}
	json.Unmarshal(apRaw, &apMap)

	for k, v := range apMap {
		if v != "" {
			m[k] = v
		}
	}

	return json.Marshal(m)
}

func handleBadResponse(resp *http.Response) error {
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("failed to read response body, err was: %v", err)
		return ErrPFBadResponse
	}
	if resp.StatusCode == http.StatusNotFound {
		log.Println("not found response body: ", string(body))
		return ErrPFNotFound
	} else {
		log.Println("bad response body: ", string(body))
		return ErrPFBadResponse
	}
}

func (as AccessState) String() string {
	return fmt.Sprintf("Authenticated: %v, Features: %v", as.Authenticated, as.Features)
}

func (fas FeatureAccessState) String() string {
	return fmt.Sprintf("Type: %v, Enabled: %v", fas.Type, fas.Enabled)
}

func (far FeatureAccessResponse) String() string {
	return fmt.Sprintf("Slug: %v, Enabled: %v", far.Slug, far.Enabled)
}
