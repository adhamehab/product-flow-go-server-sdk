package productflow

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

const testOrgId = "test-org"
const testApiEndpoint = "https://test-org.saas-public-api.zephr.com"
const overriddenApiEndpoint = "https://example.com"

func TestNewClientSuccess(t *testing.T) {

	val, err := NewClient(testOrgId)
	if err != nil {
		t.Fatalf("unexpected error")
	}

	if val.orgId != testOrgId {
		t.Fatalf("orgId was %v, expected %v", val.orgId, testOrgId)
	}

	if val.apiEndpoint != testApiEndpoint {
		t.Fatalf("apiEndpoint was %v, expected %v", val.apiEndpoint, testApiEndpoint)
	}
}

func TestNewClientNoOrg(t *testing.T) {

	_, err := NewClient("")
	if fmt.Sprint(err) != "orgId must be provided" {
		t.Fatalf("unexpected error: %v", err)
	}
}

func TestNewCustomClientSuccess(t *testing.T) {

	val, err := NewCustomClient(testOrgId, "https://example.com")
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	if val.orgId != testOrgId {
		t.Fatalf("orgId was %v, expected %v", val.orgId, testOrgId)
	}

	if val.apiEndpoint != overriddenApiEndpoint {
		t.Fatalf("apiEndpoint was %v, expected %v", val.apiEndpoint, overriddenApiEndpoint)
	}
}

func TestNewCustomClientNoOrg(t *testing.T) {

	_, err := NewCustomClient("", overriddenApiEndpoint)
	if fmt.Sprint(err) != "orgId must be provided" {
		t.Fatalf("unexpected error: %v", err)
	}
}

func TestNewCustomClientNoApiEndpoint(t *testing.T) {

	_, err := NewCustomClient(testOrgId, "")
	if fmt.Sprint(err) != "apiEndpoint must be provided" {
		t.Fatalf("unexpected error: %v", err)
	}
}

func TestClientNewAccessControllerSuccess(t *testing.T) {

	client, _ := NewCustomClient(testOrgId, "http://localhost/nowhere")

	ac, err := client.NewAccessController()
	switch {
	case err != nil:
		t.Fatalf("could not create access controller: %v", err)
	case ac.Started:
		t.Fatalf("access controller should not be started")
	}
}

func TestStartAccessControllerSuccess(t *testing.T) {

	response := []byte(`{
		"authenticated": true,
		"features": {
			"testfeature1": { "type": "STATIC", "enabled": true },
			"testfeature2": { "type": "DYNAMIC" }
		}
	}`)
	var token string
	var body map[string]interface{}
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token = r.Header.Get("Authorization")
		json.NewDecoder(r.Body).Decode(&body)

		w.WriteHeader(http.StatusOK)
		w.Write(response)
	}))
	defer svr.Close()

	client, _ := NewCustomClient(testOrgId, svr.URL)
	params := AccessParams{
		Jwt:       "123",
		Ip:        "127.0.0.1",
		UserAgent: "Mozilla/123",
		Path:      "/",
	}
	customInputs := map[string]interface{}{
		"test": true,
	}

	ac, err := client.StartAccessController(params, customInputs)
	switch {
	case err != nil:
		t.Fatalf("could not start access controller: %v", err)
	case token != "Bearer "+params.Jwt:
		t.Fatalf("authentication bearer token wrong: %v", token)
	case body["ip"] != params.Ip:
		t.Fatalf("request body wrong ip: %v", body["ip"])
	case body["userAgent"] != params.UserAgent:
		t.Fatalf("request body wrong user agent: %v", body["userAgent"])
	case body["path"] != params.Path:
		t.Fatalf("request body wrong path: %v", body["path"])
	case body["test"] != customInputs["test"]:
		t.Fatalf("request body wrong custom input test: %v", body["test"])
	case !ac.IsAuthenticated():
		t.Fatalf("user should be authenticated")
	case len(ac.AllFeatures()) != 2:
		t.Fatalf("Should be 2 features: %v", ac.AllFeatures())
	case !ac.Started:
		t.Fatalf("access controller should be started")
	}
}
