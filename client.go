package productflow

import (
	"errors"
	"fmt"
	"net/http"
)

type Client struct {
	orgId       string
	apiEndpoint string
	httpClient  *http.Client
}

// NewClient creates a new server-side Product Flow client instance with default api endpoint
func NewClient(orgId string) (*Client, error) {

	apiEndpoint := fmt.Sprintf("https://%v.saas-public-api.zephr.com", orgId)

	return NewCustomClient(orgId, apiEndpoint)
}

// NewCustomClient creates a new server-side Product Flow client instance with overridden api endpoint
func NewCustomClient(orgId, apiEndpoint string) (*Client, error) {

	if orgId == "" {
		return nil, errors.New("orgId must be provided")
	}

	if apiEndpoint == "" {
		return nil, errors.New("apiEndpoint must be provided")
	}

	client := &Client{
		orgId:       orgId,
		apiEndpoint: apiEndpoint,
		httpClient:  &http.Client{},
	}

	return client, nil
}

// NewAccessController creates a new Access Controller
func (client *Client) NewAccessController() (*AccessController, error) {

	zac, err := NewAccessController(client.apiEndpoint, client.httpClient)
	if err != nil {
		return nil, err
	}

	return zac, err
}

// StartAccessController creates and starts a new Access Controller to determine the current state of a user
//
// Access params and custom inputs are passed through to the new access controller.
func (client *Client) StartAccessController(
	params AccessParams,
	customInputs map[string]interface{},
) (*AccessController, error) {

	zac, err := NewAccessController(client.apiEndpoint, client.httpClient)
	if err != nil {
		return nil, err
	}

	err = zac.Start(params, customInputs)
	if err != nil {
		return nil, err
	}

	return zac, err
}
